package com.example.mediator.demo.pipelinr

import an.awesome.pipelinr.Command
import org.springframework.stereotype.Component

@Component
class DoSmthCommandHandler: Command.Handler<DoSmthCommand, String> {

    override fun handle(command: DoSmthCommand): String {
        println("Do somethinf to user: ${command.userName}")
        return "smth"
    }
}