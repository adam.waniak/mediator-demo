package com.example.mediator.demo.pipelinr

import an.awesome.pipelinr.Notification
import org.springframework.stereotype.Component

@Component
class UserCreatedNotificationHandler1: Notification.Handler<UserCreatedNotification> {

    override fun handle(notification: UserCreatedNotification) {
        println("${Thread.currentThread().name} Start UserCreatedNotificationHandler1")
        Thread.sleep(10000)
        println("${Thread.currentThread().name} Done UserCreatedNotificationHandler1")
    }
}

@Component
class UserCreatedNotificationHandler2: Notification.Handler<UserCreatedNotification> {

    override fun handle(notification: UserCreatedNotification) {
        println("${Thread.currentThread().name} Start UserCreatedNotificationHandler2")
        Thread.sleep(5000)
        println("${Thread.currentThread().name} Done UserCreatedNotificationHandler2")
    }
}