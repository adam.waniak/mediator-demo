package com.example.mediator.demo.pipelinr

import an.awesome.pipelinr.Command
import com.example.mediator.demo.model.UserDto


data class GetUserByEmailQuery(val email: String): Command<UserDto>


