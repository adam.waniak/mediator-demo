package com.example.mediator.demo.pipelinr

import an.awesome.pipelinr.Command
import com.example.mediator.demo.model.UserDto
import com.example.mediator.demo.model.UserRepository
import org.springframework.stereotype.Component

@Component
internal class GetUserByEmailQueryHandler(private val userRepository: UserRepository):
    Command.Handler<GetUserByEmailQuery, UserDto> {

    override fun handle(query: GetUserByEmailQuery): UserDto {
        return userRepository.getByEmail(query.email).toUserDto()
    }
}