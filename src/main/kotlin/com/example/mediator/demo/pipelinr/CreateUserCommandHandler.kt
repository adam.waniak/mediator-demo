package com.example.mediator.demo.pipelinr

import an.awesome.pipelinr.Command
import an.awesome.pipelinr.Pipeline
import com.example.mediator.demo.model.User
import com.example.mediator.demo.model.UserDto
import com.example.mediator.demo.model.UserRepository
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component

@Component
internal class CreateUserCommandHandler(
    private val userRepository: UserRepository,
    private val pipeline: Pipeline
) :
    Command.Handler<CreateUserCommand, UserDto> {
    val passwordEncoder = BCryptPasswordEncoder()
    override fun handle(command: CreateUserCommand): UserDto {
        val savedUser = userRepository.save(
            User(
                name = command.name,
                email = command.email,
                password = passwordEncoder.encode(command.password)
            )
        )
        pipeline.send(DoSmthCommand(savedUser.name))
        pipeline.send(UserCreatedNotification(savedUser.toUserDto()))
        return savedUser.toUserDto()
    }
}