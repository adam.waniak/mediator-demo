package com.example.mediator.demo.pipelinr

import an.awesome.pipelinr.Command
import com.example.mediator.demo.model.UserDto

data class CreateUserCommand(val name: String, val email: String, val password: String): Command<UserDto> {
    override fun toString(): String {
        return "CreateUserCommand(name='$name', email='$email')"
    }
}


