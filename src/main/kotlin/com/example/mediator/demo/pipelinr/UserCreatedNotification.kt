package com.example.mediator.demo.pipelinr

import an.awesome.pipelinr.Notification
import com.example.mediator.demo.model.UserDto

data class UserCreatedNotification(val userDto: UserDto): Notification