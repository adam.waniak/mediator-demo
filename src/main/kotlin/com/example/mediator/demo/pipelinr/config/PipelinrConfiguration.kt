package com.example.mediator.demo.pipelinr.config

import an.awesome.pipelinr.*
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.binder.jvm.ExecutorServiceMetrics
import io.micrometer.core.instrument.simple.SimpleMeterRegistry
import io.opentracing.Span
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.ObjectProvider
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.ThreadFactory
import java.util.concurrent.atomic.AtomicInteger
import io.opentracing.Tracer
import io.opentracing.log.Fields
import io.opentracing.mock.MockTracer
import io.opentracing.tag.Tags

@Configuration
internal class PipelinrConfiguration {
    private val LOG = LoggerFactory.getLogger(javaClass)

    @Bean
    @ConditionalOnMissingBean
    fun metricRegistry() = SimpleMeterRegistry()

    @Bean
    @ConditionalOnMissingBean(name = ["eventPublisherScheduler"])
    fun eventPublisherScheduler(meterRegistry: MeterRegistry): ExecutorService {
        val counter = AtomicInteger()
        val factory = ThreadFactory {
            val threadName = "event-${counter.incrementAndGet()}"
            Thread(it).apply {
                name = threadName
                uncaughtExceptionHandler = Thread.UncaughtExceptionHandler { thread, throwable ->
                    LOG.error("Uncaught error in thread={}", thread, throwable)
                }
            }
        }
        val newCachedThreadPool = Executors.newCachedThreadPool(factory)
        return ExecutorServiceMetrics.monitor(meterRegistry, newCachedThreadPool, "eventPublisherScheduler")
    }

    @Bean
    fun pipeline(
        commandHandlers: ObjectProvider<Command.Handler<*, *>>,
        notificationHandlers: ObjectProvider<Notification.Handler<*>>,
        commandMiddlewares: ObjectProvider<Command.Middleware>,
        notificationMiddlewares: ObjectProvider<Notification.Middleware>,
        eventPublisherScheduler: ExecutorService
    ): Pipeline {
        return Pipelinr()
            .with(commandHandlers::stream)
            .with(notificationHandlers::stream)
            .with(commandMiddlewares::stream)
            .with(notificationMiddlewares::stream)
            .with(fun(): NotificationHandlingStrategy {
                return ParallelNoWait(eventPublisherScheduler)
            })


    }

    @Bean
    fun loggingMiddleware(): Command.Middleware {
        return object : Command.Middleware {
            override fun <R : Any, C : Command<R>> invoke(command: C, next: Command.Middleware.Next<R>): R {
                println("${Thread.currentThread().name} Starting process with command $command")
                val invoke = next.invoke()
                println("${Thread.currentThread().name} Ending process with request $command")
                return invoke
            }
        }
    }

    @Bean
    fun tracer(): Tracer {
        return MockTracer()
    }

    @Bean
    fun commandTracingMiddleware(tracer: Tracer): Command.Middleware {
        return object : Command.Middleware {
            override fun <R : Any, C : Command<R>> invoke(command: C, next: Command.Middleware.Next<R>): R {
                val operationName = command.javaClass.simpleName
                tracer.buildSpan(operationName)
                    .withTag(Tags.COMPONENT.key, "handler")
                    .withTag("command", command.javaClass.name)
                    .start().activate(tracer) {
                        return next.invoke()
                    }
            }
        }
    }

    @Bean
    fun notificationTracingMiddleware(tracer: Tracer): Notification.Middleware {
        return object : Notification.Middleware {
            override fun <N : Notification> invoke(notification: N, next: Notification.Middleware.Next) {
                val operationName = notification.javaClass.simpleName
                tracer.buildSpan(operationName)
                    .withTag(Tags.COMPONENT.key, "notification")
                    .withTag("notification", notification.javaClass.name)
                    .start().activate(tracer) {
                        return next.invoke()
                    }
            }
        }
    }


}

inline fun <T> Span.activate(tracer: Tracer, block: () -> T): T {
    // see TracedHystrixCommand
    try {
        tracer.activateSpan(this).use {
            return block()
        }
    } catch (ex: Throwable) {
        setErrorAndLog(ex)
        throw ex
    } finally {
        finish()
    }
}

fun Span.setErrorAndLog(ex: Throwable) {
    // see onError in SpanJmsDecorator
    setTag(Tags.ERROR, true)
    log(
        mapOf(
            Fields.EVENT to Tags.ERROR.key,
            Fields.ERROR_KIND to ex.javaClass.name,
            Fields.MESSAGE to ex.message,
            Fields.ERROR_OBJECT to ex,
            Fields.STACK to ex.stackTraceToString()
        )
    )
}