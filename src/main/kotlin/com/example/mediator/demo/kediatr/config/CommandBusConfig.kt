package com.example.mediator.demo.kediatr.config

import com.trendyol.kediatr.CommandBus
import com.trendyol.kediatr.CommandBusBuilder
import com.trendyol.kediatr.ParallelNoWaitPublishStrategy
import com.trendyol.kediatr.spring.KediatrSpringBeanProvider
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

@Configuration
class CommandBusConfig {


    @Bean
    @Primary
    fun commandBusWithCustomPublishStrategy(kediatrSpringBeanProvider: KediatrSpringBeanProvider): CommandBus {
        return CommandBusBuilder(kediatrSpringBeanProvider).withPublishStrategy(ParallelNoWaitPublishStrategy()).build()
    }
}