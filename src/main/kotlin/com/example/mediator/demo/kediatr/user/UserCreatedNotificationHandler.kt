package com.example.mediator.demo.kediatr.user

import com.trendyol.kediatr.NotificationHandler
import org.springframework.stereotype.Component

@Component
class UserCreatedNotificationHandler1Kediatr: NotificationHandler<UserCreatedNotificationKediatr> {

    override fun handle(notification: UserCreatedNotificationKediatr) {
        println("${Thread.currentThread().name} Start UserCreatedNotificationHandler1")
        Thread.sleep(10000)
        println("${Thread.currentThread().name} Done UserCreatedNotificationHandler1")
    }
}

@Component
class UserCreatedNotificationHandler2Kediatr: NotificationHandler<UserCreatedNotificationKediatr> {

    override fun handle(notification: UserCreatedNotificationKediatr) {
        println("${Thread.currentThread().name} Start UserCreatedNotificationHandler2")
        Thread.sleep(5000)
        println("${Thread.currentThread().name} Done UserCreatedNotificationHandler2")
    }
}