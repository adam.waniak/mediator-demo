package com.example.mediator.demo.kediatr.user

import com.example.mediator.demo.model.User
import com.example.mediator.demo.model.UserRepository
import com.trendyol.kediatr.CommandBus
import com.trendyol.kediatr.CommandHandler
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component

@Component
internal class CreateUserCommandHandlerKediatr(
    private val userRepository: UserRepository,
    private val commandBus: CommandBus
) :
    CommandHandler<CreateUserCommandKediatr> {
    val passwordEncoder = BCryptPasswordEncoder()
    override fun handle(command: CreateUserCommandKediatr) {
        val savedUser = userRepository.save(
            User(
                name = command.name,
                email = command.email,
                password = passwordEncoder.encode(command.password)
            )
        )
        commandBus.executeCommand(DoSmthCommandKediatr(savedUser.name))
        commandBus.publishNotification(UserCreatedNotificationKediatr(savedUser.toUserDto()))

    }
}