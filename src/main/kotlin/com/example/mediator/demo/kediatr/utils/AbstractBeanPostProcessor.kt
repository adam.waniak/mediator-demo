package com.example.mediator.demo.kediatr.utils

import org.springframework.beans.factory.config.BeanPostProcessor

class ProcessBeforeInitializationProcessor<T>(
    private val matchingType: Class<T>,
    private val postProcess: (T) -> T
) : BeanPostProcessor {

    override fun postProcessBeforeInitialization(bean: Any, beanName: String): Any? {
        return if (matchingType.isAssignableFrom(bean.javaClass)) {
            @Suppress("UNCHECKED_CAST")
            postProcess(bean as T)
        } else {
            super.postProcessBeforeInitialization(bean, beanName)
        }
    }
}

class ProcessAfterInitializationProcessor<T>(
    private val matchingType: Class<T>,
    private val postProcess: (T) -> T
) : BeanPostProcessor {

    override fun postProcessAfterInitialization(bean: Any, beanName: String): Any? {
        return if (matchingType.isAssignableFrom(bean.javaClass)) {
            @Suppress("UNCHECKED_CAST")
            postProcess(bean as T)
        } else {
            super.postProcessAfterInitialization(bean, beanName)
        }
    }
}
