package com.example.mediator.demo.kediatr.user

import com.example.mediator.demo.model.UserDto
import com.example.mediator.demo.model.UserRepository
import com.trendyol.kediatr.QueryHandler
import org.springframework.stereotype.Component

@Component
internal class GetUserByEmailQueryHandlerKediatr(private val userRepository: UserRepository):
    QueryHandler<GetUserByEmailQueryKediatr, UserDto> {

    override fun handle(query: GetUserByEmailQueryKediatr): UserDto {
        return userRepository.getByEmail(query.email).toUserDto()
    }
}