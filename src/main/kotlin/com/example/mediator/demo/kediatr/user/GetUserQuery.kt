package com.example.mediator.demo.kediatr.user

import com.example.mediator.demo.model.UserDto
import com.trendyol.kediatr.Query


data class GetUserByEmailQueryKediatr(val email: String): Query<UserDto>


