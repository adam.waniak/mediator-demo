package com.example.mediator.demo.kediatr.user

import com.example.mediator.demo.model.UserDto
import com.trendyol.kediatr.Notification

data class UserCreatedNotificationKediatr(val userDto: UserDto): Notification