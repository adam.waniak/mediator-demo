package com.example.mediator.demo.kediatr.user

import com.trendyol.kediatr.Command
data class CreateUserCommandKediatr(val name: String, val email: String, val password: String): Command {
    override fun toString(): String {
        return "CreateUserCommand(name='$name', email='$email')"
    }
}


