package com.example.mediator.demo.kediatr.utils

import com.trendyol.kediatr.AsyncPipelineBehavior
import com.trendyol.kediatr.PipelineBehavior
import org.springframework.stereotype.Component

@Component
class AsyncProcessingPipeline : AsyncPipelineBehavior {
    override suspend fun <TRequest> preProcess(request: TRequest) {
        println("${Thread.currentThread().name} Starting process with request $request")
    }
    override suspend fun <TRequest> postProcess(request: TRequest) {
        println("${Thread.currentThread().name} Ending process with request $request")
    }
    override suspend fun <TRequest, TException : Exception> handleException(request: TRequest, exception: TException) {
        println("Some exception occurred during process. Error: $exception")
    }
}

@Component
class ProcessingPipeline : PipelineBehavior {
    override fun <TRequest> preProcess(request: TRequest) {
        println("${Thread.currentThread().name} Starting process with request $request")
    }
    override fun <TRequest> postProcess(request: TRequest) {
        println("${Thread.currentThread().name} Ending process with request $request")
    }

    override fun <TRequest, TException : Exception> handleExceptionProcess(request: TRequest, exception: TException) {
        println("Some exception occurred during process. Error: $exception")
    }
}