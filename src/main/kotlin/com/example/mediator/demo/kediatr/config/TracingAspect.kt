package com.example.mediator.demo.kediatr.config

import com.example.mediator.demo.pipelinr.config.activate
import io.opentracing.Tracer
import io.opentracing.tag.Tags
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.springframework.stereotype.Component


@Aspect
@Component
class TracingAspect(private val tracer: Tracer) {


    @Around("within(com.trendyol.kediatr.CommandHandler+) && execution(* handle(..))")
    fun traceCommandHandler(joinPoint: ProceedingJoinPoint): Any? {
        val operationName = joinPoint.target::class.java.toString()
        tracer.buildSpan(operationName)
            .withTag(Tags.COMPONENT.key, "handler")
            .withTag("command", operationName)
            .start().activate(tracer) {
                return joinPoint.proceed()
            }
    }

    @Around("within(com.trendyol.kediatr.QueryHandler+) && execution(* handle(..))")
    fun traceQueryHandler(joinPoint: ProceedingJoinPoint): Any? {
        val operationName = joinPoint.target::class.java.toString()
        tracer.buildSpan(operationName)
            .withTag(Tags.COMPONENT.key, "handler")
            .withTag("query", operationName)
            .start().activate(tracer) {
                return joinPoint.proceed()
            }
    }

    @Around("within(com.trendyol.kediatr.NotificationHandler+) && execution(* handle(..))")
    fun traceNotificationHandler(joinPoint: ProceedingJoinPoint): Any? {
        val operationName = joinPoint.target::class.java.toString()
        tracer.buildSpan(operationName)
            .withTag(Tags.COMPONENT.key, "handler")
            .withTag("notification", operationName)
            .start().activate(tracer) {
                return joinPoint.proceed()
            }
    }
}