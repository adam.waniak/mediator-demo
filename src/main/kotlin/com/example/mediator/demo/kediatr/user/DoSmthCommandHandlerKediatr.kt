package com.example.mediator.demo.kediatr.user

import com.trendyol.kediatr.CommandHandler
import org.springframework.stereotype.Component

@Component
class DoSmthCommandHandlerKediatr: CommandHandler<DoSmthCommandKediatr> {

    override fun handle(command: DoSmthCommandKediatr) {
        println("Do somethinf to user: ${command.userName}")
    }
}