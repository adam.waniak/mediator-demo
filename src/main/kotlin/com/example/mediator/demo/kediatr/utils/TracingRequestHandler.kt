package com.example.mediator.demo.kediatr.utils

import com.trendyol.kediatr.Command
import com.trendyol.kediatr.CommandHandler
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


//Decorating KediatR handlers causes an inability to fetch command argument via reflection

//internal class TracingRequestHandlerDecorator<TIn : Command>(
//    private val innerHandler: CommandHandler<TIn>
//) : CommandHandler<TIn> {
//    override fun handle(command: TIn) {
//        val operationName = command.javaClass.simpleName
//        println("Command handler tracing decorator, operationName: $operationName")
//        return innerHandler.handle(command)
//    }
//}
//
//@Configuration
//internal class TracingRequestHandlerConfiguration {
//    @Bean
//    @ConditionalOnBean(CommandHandler::class)
//    fun decorateRequestHandlerWithTracing() =
//        ProcessAfterInitializationProcessor(CommandHandler::class.java) { handler ->
//            TracingRequestHandlerDecorator(handler)
//        }
//}
