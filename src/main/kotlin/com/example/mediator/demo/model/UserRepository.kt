package com.example.mediator.demo.model

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
internal interface UserRepository: JpaRepository<User, Long> {

    fun getByEmail(email: String): User
}