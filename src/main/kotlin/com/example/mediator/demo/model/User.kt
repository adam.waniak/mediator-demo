package com.example.mediator.demo.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

data class UserDto(val id: Long, val name: String, val email: String)

@Entity
@Table(name = "my_user_table")
internal class User(
    var name: String,
    var email: String,
    var password: String,
    @Id @GeneratedValue
    var id: Long? = 0
    ) {

    fun toUserDto(): UserDto {
        return UserDto(id!!, name, email)
    }
}