package com.example.mediator.demo

import an.awesome.pipelinr.Pipeline
import com.example.mediator.demo.kediatr.user.CreateUserCommandKediatr
import com.example.mediator.demo.kediatr.user.GetUserByEmailQueryKediatr
import com.example.mediator.demo.pipelinr.GetUserByEmailQuery
import com.trendyol.kediatr.CommandBus
import io.opentracing.Tracer
import io.opentracing.mock.MockTracer
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class DemoApplicationTests {

    @Autowired
    lateinit var commandBus: CommandBus

    @Autowired
    lateinit var pipeline: Pipeline

    @Autowired
    lateinit var tracer: Tracer

    @Test
    fun kediatRTest() {
        commandBus.executeCommand(CreateUserCommandKediatr("Bob", "bob@domain.com", "strongPassword"))
        val userDto = commandBus.executeQuery(GetUserByEmailQueryKediatr("bob@domain.com"))
        Thread.sleep(10000)
        println((tracer as MockTracer).finishedSpans())
    }

    @Test
    fun pipelinRTest() {
        pipeline.send(com.example.mediator.demo.pipelinr.CreateUserCommand("Bob", "bob@domain.com", "strongPassword"))
        val userDto = pipeline.send(GetUserByEmailQuery("bob@domain.com"))
        Thread.sleep(12000)
        println((tracer as MockTracer).finishedSpans())

    }

}
